/* Utility function to find out if an obect already extists before trying to
 * create it */
create or replace function pglog.is_object_already_there(schema_name text, object_name text)
returns boolean as
$body$
  declare
    result boolean;
  begin
    execute format(
      $$select case when count(pg_class.oid) > 0 then true
        else false
      end
      from pg_class
        inner join pg_namespace on pg_class.relnamespace = pg_namespace.oid
      where relname = %L
        and nspname = %L$$, object_name, schema_name) into result;
    return result;
  end;
$body$
language 'plpgsql';

