create or replace procedure pglog.create_view_sessions_stats(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'sessions_stats') then
      execute format($$drop %s view %I.sessions_stats$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.sessions_stats as (
        select
          min(substring(message from  '\d+:\d{2}:\d{2}\.\d{3}')::interval) as min_duration,
          max(substring(message from  '\d+:\d{2}:\d{2}\.\d{3}')::interval) as max_duration,
          avg(substring(message from  '\d+:\d{2}:\d{2}\.\d{3}')::interval) as avg_duration,
          user_name,
          database_name
        from %I.pglog
        where message ~ '^disconnection: session time: '
        group by user_name,
          database_name
      )$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;

