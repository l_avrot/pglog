create or replace procedure pglog.create_view_connections_report(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'connections_report') then
      execute format($$drop %s view %I.connections_report$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.connections_report as (
        with connection_messages(user_name, database_name, log_time) as (
          select
            user_name,
            database_name,
            log_time
          from %I.pglog
          where message ~ '^connection authorized: '
        ),
        logtimes(user_name, database_name, log_time, next_log_time) as (
          select
            user_name,
            database_name,
            log_time,
            lead(log_time) over (
              partition by user_name, database_name
              order by log_time)
          from connection_messages
        )
        select
          user_name,
          database_name,
          avg(next_log_time - log_time) as frequency,
          count(log_time) as connection_number
        from logtimes
        where next_log_time is not null
        group by user_name,
          database_name
      )$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;

