/* This procedure will create a foreign table from a given csv file */
create or replace procedure pglog.create_foreign_table_from_file(filename text,
  directory text,
  logversion integer,
  schema_name text default 'pglog') as
$body$
  declare
    fdw_name text;
    bounds record;
  begin
    execute format($$select lower(regexp_replace(regexp_replace(%L, '\.csv$',''),'\W+',''))$$, filename) into fdw_name;
    if not pglog.is_object_already_there(schema_name, fdw_name) then
      if logversion < 13 then
        /* The following lines will be removed when Postgres 12 won't be
         * supported anymore */
        execute format(
          $$create foreign table %I.%I (
              log_time timestamp(3) with time zone,
              user_name text,
              database_name text,
              process_id integer,
              connection_from text,
              session_id text,
              session_line_num bigint,
              command_tag text,
              session_start_time timestamp with time zone,
              virtual_transaction_id text,
              transaction_id bigint,
              error_severity text,
              sql_state_code text,
              message text,
              detail text,
              hint text,
              internal_query text,
              internal_query_pos integer,
              context text,
              query text,
              query_pos integer,
              location text,
              application_name text
          ) server pglog
          options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       elsif logversion < 14 then
          /* Postgres 13 added the backend_type column */
          execute format(
            $$create foreign table %I.%I (
                log_time timestamp(3) with time zone,
                user_name text,
                database_name text,
                process_id integer,
                connection_from text,
                session_id text,
                session_line_num bigint,
                command_tag text,
                session_start_time timestamp with time zone,
                virtual_transaction_id text,
                transaction_id bigint,
                error_severity text,
                sql_state_code text,
                message text,
                detail text,
                hint text,
                internal_query text,
                internal_query_pos integer,
                context text,
                query text,
                query_pos integer,
                location text,
                application_name text,
                backend_type text
            ) server pglog
            options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       else
          /* Postgres 14 has inherited the backend_type column from Postgres 13
           * and added the leader_pid and the query_id columns.*/
         execute format(
           $$create foreign table %I.%I (
               log_time timestamp(3) with time zone,
               user_name text,
               database_name text,
               process_id integer,
               connection_from text,
               session_id text,
               session_line_num bigint,
               command_tag text,
               session_start_time timestamp with time zone,
               virtual_transaction_id text,
               transaction_id bigint,
               error_severity text,
               sql_state_code text,
               message text,
               detail text,
               hint text,
               internal_query text,
               internal_query_pos integer,
               context text,
               query text,
               query_pos integer,
               location text,
               application_name text,
               backend_type text,
               leader_pid integer,
               query_id bigint
           ) server pglog
           options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       end if;
      execute format(
        $$select min(log_time) as min_time,
          (max(log_time) + interval '1ms') as max_time
        from %I.%I$$, schema_name, fdw_name) into bounds;
      /* There's not need to add an empty table as a partition + which bound
       * would you pick for the empty partition? */
      if bounds.min_time is not null
      then
        execute format($$alter table %I.pglog attach partition %I.%I for values from (%L) to (%L)$$,
          schema_name,
          schema_name,
          fdw_name,
          bounds.min_time,
          bounds.max_time);
      end if;
    else
      raise notice $$There is already an object called % in schema %.$$, fdw_name, schema_name;
    end if;
  end;
$body$
language plpgsql;

