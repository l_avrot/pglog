create or replace procedure pglog.create_views_error_analyze(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
    severity text[];
    label text;
  begin
    severity := array['WARNING', 'ERROR', 'FATAL', 'PANIC'];
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    foreach label in array severity
    loop
      if pglog.is_object_already_there(schema_name, 'error_analyze' || lower(label)) then
        execute format($$drop %s view %I.error_report_%s $$, mat, schema_name, lower(label));
      end if;
      execute format(
        $$create %s view %I.error_analyze_%s as (select
            message,
            count(1),
            min(log_time),
            max(log_time)
          from
            %I.pglog
          where error_severity = %L
          group by message
          order by count(1) desc)$$,
        mat, schema_name, lower(label), schema_name, label);
    end loop;
  end
$body$
language plpgsql;


