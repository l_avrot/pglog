create or replace procedure pglog.create_view_checkpoints_stats_by_hour(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'checkpoints_stats_by_hour') then
      execute format($$drop %s view %I.checkpoints_stats_by_hours$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.checkpoints_stats_by_hour as (
        with checkpoint_start(rnum, process_id, log_time, session_line_num, message) as
        (select
          row_number() over (order by log_time, process_id, session_line_num) as rnum,
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint starting: time'
        ),
        checkpoint_stop(process_id, log_time, session_line_num, message) as
        (select
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint complete:'
        )
        select
          extract(hour from s2.log_time),
          avg(s2.log_time - s1.log_time) as avg_checkpoint_freq,
          percentile_cont(0.5) within group(order by s2.log_time - s1.log_time) as median_checkpoint_freq,
          avg(e1.log_time - s1.log_time) as avg_checkpoint_duration,
          percentile_cont(0.5) within group(order by e1.log_time - s1.log_time) as median_checkpoint_duration
        from checkpoint_start s1
          inner join
            checkpoint_start s2
          on s1.rnum + 1 = s2.rnum
          inner join
            checkpoint_stop e1
          on s1.process_id = e1.process_id
          and s1.session_line_num + 1 = e1.session_line_num
          inner join
            checkpoint_stop e2
          on s2.process_id = e2.process_id
          and s2.session_line_num + 1 = e2.session_line_num
         group by extract(hour from s2.log_time))$$,
       mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;
