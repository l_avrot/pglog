/* This procedure will create a logtable from a given csv file 
 * If the materialized boolean is false, it will create a FDW
 * If the materialized boolean is true, it will create a real table */
create or replace procedure pglog.create_logtable_from_file(
  filename text,
  directory text,
  logversion integer,
  schema_name text default 'pglog',
  materialized boolean default false
  ) as
$body$
  declare
    is_new boolean := false;
  begin
    if not pglog.is_object_already_there(schema_name, 'pglog')
    then
      execute format($$call pglog.create_logtable(%L,%L::integer)$$, schema_name, logversion);
        is_new := true;
    end if;
    if materialized
    then
      execute format($$call pglog.create_real_table_from_file(%L, %L, %L::integer, %L)$$, filename, directory, logversion, schema_name);
      if is_new
      then
        execute format($$create index on %I.pglog(error_severity)$$, schema_name);
        raise notice $$For performance reason, you might want to vacuum %.pglog after load$$, schema_name;
      end if;
    else
      execute format($$call pglog.create_foreign_table_from_file(%L, %L, %L::integer, %L)$$, filename, directory, logversion, schema_name);
    end if;
    execute format($$call pglog.create_all_views(%L, %L::boolean)$$, schema_name, materialized);
  end;
$body$
language 'plpgsql';

