create or replace procedure pglog.create_view_error_report(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'error_report ') then
      execute format($$drop %s view %I.error_report $$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.error_report as (select
        error_severity,
        count(*) as nb,
        round(100. * count(*) / sum(count(*)) over (),2)
      from %I.pglog
        inner join
        (values (0, 'PANIC'),
          (1,'FATAL'),
          (2,'ERROR'),
          (3,'WARNING'),
          (4,'LOG'),
          (5,'NOTICE'),
          (6,'INFO'),
          (7,'DEBUG')) as severity(level,name)
        on %I.pglog.error_severity = severity.name
      group by error_severity,
        severity.level
      order by severity.level)$$, mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;

