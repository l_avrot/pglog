create or replace procedure pglog.create_view_tempfile_queries(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'tempfile_queries') then
      execute format($$drop %s view %I.tempfile_queries$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.tempfile_queries as (
        with temporary_file(session_id, session_line_num, size, query) as (
          select session_id,
            session_line_num,
            /* Doc specifies that should no unit be there, the size is in kB */
            split_part(message, ', size ', 2)::bigint*1024,
            /* remove hard values (number) */
            regexp_replace
            (
              /* remove hard values (quote delimited) */
              regexp_replace
              (
                /* remove comments */
                regexp_replace
                (
                  query,
                  '[ \t\r\n\v\f]*/\*.+\*/[ \t\r\n\v\f]*',
                  ''
                ),
                E'\'.+\'',
                '?'
              ),
              '=[ \t\r\n\v\f]*[0-9\.]+',
              '= ?'
            )
          from %I.pglog
          where error_severity = 'LOG'
            and message ~ '^temporary file:.*, size '
        )
        select count(*) as frequency,
          pg_size_pretty(percentile_disc(0.5) within group (order by size)) as query_tempfilesize_median,
          pg_size_pretty(avg(size)) as query_tempfilesize_average,
          pg_size_pretty(count(*) * avg(size)) as total_size,
          query
        from temporary_file
        group by query
        order by count(*) * avg(size) desc)$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;
