create or replace procedure pglog.create_view_autovacuum_report(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'autovacuum_report') then
      execute format($$drop %s view %I.autovacuum_report$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.autovacuum_report as (
        with autovacuum(schema_name, table_name, session_id) as (
          select
            split_part
            (
              split_part(message, '"', 2),
              '.',
              2
            ) as schema_name,
            split_part
            (
              split_part(message, '"', 2),
              '.',
              3
            ) as table_name,
            session_id
          from %I.pglog
          where error_severity = 'LOG'
            and message ~  '^automatic vacuum of table '
        ),
        autoanalyze(schema_name, table_name, session_id) as (
          select
            split_part
            (
              split_part(message, '"', 2),
              '.',
              2
            ) as schema_name,
            split_part
            (
              split_part(message, '"', 2),
              '.',
              3
            ) as table_name,
            session_id
          from %I.pglog
          where error_severity = 'LOG'
            and message ~  '^automatic analyze of table '
        ),
        stat_vacuum(schema_name, table_name, vacuum_count) as (
          select schema_name,
            table_name,
            count(*) as vacuum_count
          from autovacuum
          where table_name is not null
          group by schema_name,
           table_name
        ),
        stat_analyze(schema_name, table_name, analyze_count) as (
          select schema_name,
            table_name,
            count(*) as analyze_count
          from autoanalyze
          where table_name is not null
          group by schema_name,
           table_name
        )
        select
          /* As we can don't know if we analyze more than we vacuum and vice
           * versa, we have to find out which table and schema names are not
           * null */
          coalesce(stat_analyze.schema_name, stat_vacuum.schema_name) as schema_name,
          coalesce(stat_analyze.table_name, stat_vacuum.table_name) as table_name,
          coalesce(vacuum_count, 0) as vacuum_count,
          coalesce(analyze_count, 0) as analyze_count
        from stat_vacuum
          /* Normaly we analyze a table more frequently than we vacuum it,
           * so we have to left join */
          full join
            stat_analyze
          on stat_vacuum.schema_name = stat_analyze.schema_name
          and stat_vacuum.table_name = stat_analyze.table_name
            order by vacuum_count desc, analyze_count desc)$$, mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;

