**Do not use on a PostgreSQL production cluster. That's not what this tool is meant for!**

# pglog

`pglog` is a simple way to perform SQL queries on your log files.  Depending on
your needs, you can create foreign tables that read your csv logfiles, or you
can create real tables and load the data from the logs into them. The former is
quicker (O(1)) to start. The latter takes time to get started, O(n) in lines of
logs, and is quicker to query.

The `materialize` option copies the data into a database.  As such, it is a
snapshot of the logs as they existed when imported, i.e. does not contain
entries created later.  This is particularly helpful when analyzing log files
from another instance.  By default, `pglog` does not materialize the data.

## Prerequisites
### Configuring logs
Your logfiles need to be in the `csv` file format. See [PostgreSQL documentation
about logging](https://www.postgresql.org/docs/current/runtime-config-logging.html) for more information.

Analyzing PostgreSQL logfiles is great, but it's better if you have enough
information to play with. Therefore, I recommend setting the log parameters
with those values:

| *Parameter*                   | *Setting* |
|-----------------------------|---------|
| log_min_duration_statement  | 250 ms  |
| log_autovacuum_min_duration | 0       |
| log_checkpoints             | on      |
| log_connections             | on      |
| log_disconnections          | on      |
| log_lock_waits              | on      |
| log_temp_files              | 0       |

### Dependencies and compliance
The extension uses stored procedures. You need to use at least PostgreSQL 11.
This extension also uses the function `pg_ls_dir`, so the database user reading
the directory needs to have `execute` permission on that administration
function.

You can install this extension on your laptop to analyze PostgreSQL for
versions 9.0 and later, 9.0 being when the `application_name` column was added.

## Installation
### Pre-requisites
You need to have PostgreSQL already installed.
You need to get the extension and install it:

```sh
    git clone <pglog_project>
    cd pglog
    make install
```

### Creating the extension
Then you can create the extension

```sql
    create extension pglog cascade;
```

This installs the extension `file_fdw` if it's not already installed.  The
extension will create a schema named `pglog`.

## How to create the `pglog` partitioned table
The extension will create a `pglog` schema containing stored procedures and functions.
You need to give a file or a directory to create the foreign tables on top of
it. It will then create a `pglog` partitioned table with foreign partitions.

You can choose from among the following procedures:

* call pglog.create_logtable_from_file(file, directory, version[, schema][,
	materialized]);
* call pglog.create_logtable_from_dir(directory, version[, schema][,
	materialized]);
* call pglog.mylogs(schema[, materialized]); **-- Only for test purposes, not on production, please!**

## Dashboards
Based on the `pglog` partitioned table, you can generate dashboards.

###  Error report
This view counts errors in `pglog` by severity.

Example:

~~~
pglog=# select * from error_report;
 error_severity |   nb    | percentage
----------------+---------+------------
 FATAL          |       8 |       0.00
 ERROR          |     106 |       0.00
 WARNING        |       3 |       0.00
 LOG            | 6055776 |     100.00
(4 rows)
~~~

### Detail error analysis
The error_analyze_(warning|error|fatal|panic) views display a more detailed
analysis of the given log level. Lower severity levels tend to produce large
numbers of rows, so I recommend using appending `FETCH FIRST n ROWS` to, or
otherwise limiting, your query.

Example:

~~~
select * from error_analyze_fatal;
                       message                       | count |            min             |            max
-----------------------------------------------------+-------+----------------------------+----------------------------
 terminating connection due to administrator command |     7 | 2021-08-25 13:01:32.992+00 | 2021-08-27 09:12:44.884+00
 connection to client lost                           |     2 | 2021-08-25 12:20:24.044+00 | 2021-08-25 12:20:24.044+00
(2 rows)
~~~

### Autovacuum report
This view displays the counts of autovacuum and autoanalyze runs by table. You
might want to add a `limit n` part to your query so you don't display all your
tables in the result.

Example:

~~~
pglog=# select * from autovacuum_report limit order by vacuum_count desc fetch first 5 rows only;
 schema_name |  table_name   | vacuum_count | analyze_count
-------------+---------------+--------------+---------------
 pg_catalog  | pg_statistic  |           24 |             0
 pg_catalog  | pg_attribute  |           15 |            14
 pglog       | postgresqlwed |            8 |             7
 pg_catalog  | pg_class      |            7 |             7
 pg_catalog  | pg_type       |            6 |             6
(5 rows)
~~~

### Checkpoints
This view contains statistics about your checkpoints: how often they're
launched, and how long they take to complete.

Example:

~~~
pglog=# select * from checkpoints_stats;
 avg_checkpoint_freq | median_checkpoint_freq | avg_checkpoint_duration | median_checkpoint_duration
---------------------+------------------------+-------------------------+----------------------------
 06:01:55.491981     | 00:15:00.809           | 00:00:32.447288         | 00:00:06.6675
(1 row)
~~~

### Checkpoints by hour
Database load normally has peaks and valleys. This view shows checkpoint
statistics by hour. Once you identify the peak hours, I strongly suggest that
you craft a query to get the same stats per minute during the peak times.

Example:

~~~
pglog=# select * from checkpoints_stats_by_hour;
 date_part | avg_checkpoint_freq | median_checkpoint_freq | avg_checkpoint_duration | median_checkpoint_duration
-----------+---------------------+------------------------+-------------------------+----------------------------
         6 | 03:31:43.989333     | 00:04:59.941           | 00:00:06.375            | 00:00:04.431
         7 | 22:36:05.281917     | 00:15:00.809           | 00:00:17.00225          | 00:00:03.916
         8 | 00:21:22.5615       | 00:15:01.661           | 00:00:17.9445           | 00:00:09.3765
         9 | 00:39:21.674167     | 00:30:17.2415          | 00:00:33.430833         | 00:00:11.2735
        10 | 00:16:09.9175       | 00:16:09.9175          | 00:00:06.032            | 00:00:06.032
        11 | 00:37:25.941667     | 00:12:22.328           | 00:01:40.044667         | 00:02:29.37
        12 | 01:11:27.7846       | 00:40:00.957           | 00:01:05.7842           | 00:00:16.016
        13 | 00:15:19.55725      | 00:07:56.6295          | 00:01:18.24225          | 00:01:21.253
        15 | 04:13:58.037        | 04:13:58.037           | 00:02:30.109            | 00:02:30.109
        16 | 02:58:23.038333     | 00:25:00.427           | 00:00:05.165667         | 00:00:05.012
        17 | 00:15:00.34025      | 00:07:30.346           | 00:00:04.27425          | 00:00:03.195
        19 | 01:02:31.026        | 01:02:31.026           | 00:00:06.4115           | 00:00:06.4115
        20 | 00:09:59.441        | 00:09:59.441           | 00:00:06.241            | 00:00:06.241
(13 rows)
~~~

### Temp file report
Temp files are written when PostgreSQL spills to disc, which is often a signal
that `work_mem` is set too low and needs to be raised, at least for those
things causing these spills.

Example:

~~~
pglog=# select * from tempfile_report;
   min   |  max   | global_average |   10%   |   20%   |   30%   |   40% |  50%  |  60%  |  70%  |  80%   |  90%   |  100%
---------+--------+----------------+---------+---------+---------+---------+-------+-------+-------+--------+--------+--------
 8192 kB | 374 GB | 57 GB          | 5720 MB | 5720 MB | 5720 MB | 6000 MB | 32 GB | 33 GB | 36 GB | 142 GB | 144 GB | 374 GB
(1 row)
~~~

### Temp file queries
It often happens that most temp files are used by one or two specific queries.
In that case, you can set `work_mem` to a higher value when running those
queries in the narrow context of a session rather than in a wider context,
which could have unpleasant impacts elsewhere.

Example:
~~~
pglog=# select * from tempfile_queries order by frequency fetch first 2 rows only\gx
-[ RECORD 1 ]--------------+----------------------------------------------------------------------
frequency                  | 33
query_tempfilesize_median  | 142 GB
query_tempfilesize_average | 142 GB
total_size                 | 4689 GB
query                      | create materialized view pglog.top_n_queries as (                    +
                           |       with queries as (                                              +
                           |       select                                                         +
                           |         split_part(message, ?)                                       +
                           |     )                                                                +
                           |     select query,                                                    +
                           |       count(1),                                                      +
                           |       avg(duration) as average,                                      +
                           |       percentile_disc(0.5) within group (order by duration) as median+
                           |     from queries                                                     +
                           |     group by query                                                   +
                           |     order by average desc                                            +
                           |     )
-[ RECORD 2 ]--------------+----------------------------------------------------------------------
frequency                  | 154
query_tempfilesize_median  | 6000 MB
query_tempfilesize_average | 30 GB
total_size                 | 4655 GB
query                      | call create_mylogs (?,true);
~~~

### Top n slow queries
This view shows the slowest n queries in `pglog`. Narrow this down
unless you want to see all your slow queries.

Example:

~~~
pglog=# select *
from top_n_slowest_queries
order by "average" desc
fetch first 5 rows only;
-[ RECORD 1 ]----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
query   | with queries as ( select split_part(message, ? ) ) select query, count ? , avg(duration) as average, percentile_disc ? within group (order by duration) as median from queries group by query order by average desc limit ?
count   | 1
average | 26384.688000000000
median  | 26384.688
-[ RECORD 2 ]----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
query   | with total as ( select count ? as total from pglog.pglog ) select error_severity, count ? as nb, round(count ? /total::numeric ? 2) as percentage from pglog.pglog inner join (values ? ? )) as severity(level,name) on pglog.pglog.error_severity = severity.name, total group by error_severity, severity.level, total order by severity.level;
count   | 1
average | 24090.557000000000
median  | 24090.557
-[ RECORD 3 ]----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
query   | with total as ( select count ? as total from pglog.pglog ) select error_severity, count ? as nb, round(count ? /total::numeric ? 2) as percentage from pglog.pglog, total group by error_severity, total order by percentage desc;
count   | 2
average | 23530.949500000000
median  | 23210.202
-[ RECORD 4 ]----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
query   | UPDATE pgbench_branches SET bbalance = bbalance + ? WHERE bid = ?
count   | 173009
average | 2.5784063256824789
median  | 1.666
-[ RECORD 5 ]---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
query   | UPDATE pgbench_tellers SET tbalance = tbalance + ? WHERE tid = ?
count   | 173009
average | 1.9884613054812177
median  | 0.593
~~~

### Session statistics
This view shows some statistics about the session duration.

Example:

~~~
pglog=# select * from sessions_stats ;
 min_duration | max_duration  |  avg_duration   | user_name | database_name 
--------------+---------------+-----------------+-----------+---------------
 00:00:00.033 | 00:41:05.38   | 00:05:32.636933 | postgres  | mybench
 00:02:41.311 | 156:33:45.673 | 43:09:49.903    | postgres  | pglog
 00:00:03.538 | 00:00:06.231  | 00:00:04.8845   | postgres  | postgres
 00:00:00.003 | 00:00:00.013  | 00:00:00.007333 | postgres  | template1
(4 rows)
~~~

### Connections report
This view shows details of connections frequency per user and database.

Example:

~~~
pglog=# select * from connections_report ;
 user_name | database_name |        frequency        | connection_number 
-----------+---------------+-------------------------+-------------------
 postgres  | mybench       | 00:23:57.488429         |                14
 postgres  | pglog         | 10 days 12:05:08.038667 |                 3
 postgres  | postgres      | 00:04:20.521            |                 1
 postgres  | template1     | 00:00:00.519            |                 2
(4 rows)
~~~

## Uninstalling
Simply use the `drop extension` statement (with or without the `cascade` option,
depending on what you want to achieve).

## License
This project is under the PostgreSQL license.
