--complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pglog" to load this file. \quit

create server pglog foreign data wrapper file_fdw;

create schema pglog;

/* Utility function to find out if an obect already extists before trying to
 * create it */
create or replace function pglog.is_object_already_there(schema_name text, object_name text)
returns boolean as
$body$
  declare
    result boolean;
  begin
    execute format(
      $$select case when count(pg_class.oid) > 0 then true
        else false
      end
      from pg_class
        inner join pg_namespace on pg_class.relnamespace = pg_namespace.oid
      where relname = %L
        and nspname = %L$$, object_name, schema_name) into result;
    return result;
  end;
$body$
language 'plpgsql';

create or replace procedure pglog.create_all_views(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  begin
    execute format($$call pglog.create_view_top_n_query(%L, %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_error_report(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_tempfile_stats(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_tempfile_queries(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_autovacuum_report(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_checkpoints_stats(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_checkpoints_stats_by_hour(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_views_error_analyze(%L,  %L::boolean)$$,
      schema_name, materialized);
  end
$body$
language plpgsql;

/* This procedure will create a foreign table from a given csv file */
create or replace procedure pglog.create_foreign_table_from_file(filename text,
  directory text,
  logversion integer,
  schema_name text default 'pglog') as
$body$
  declare
    fdw_name text;
    bounds record;
  begin
    execute format($$select lower(regexp_replace(regexp_replace(%L, '\.csv$',''),'\W+',''))$$, filename) into fdw_name;
    if not pglog.is_object_already_there(schema_name, fdw_name) then
      if logversion < 13 then
        /* The following lines will be removed when Postgres 12 won't be
         * supported anymore */
        execute format(
          $$create foreign table %I.%I (
              log_time timestamp(3) with time zone,
              user_name text,
              database_name text,
              process_id integer,
              connection_from text,
              session_id text,
              session_line_num bigint,
              command_tag text,
              session_start_time timestamp with time zone,
              virtual_transaction_id text,
              transaction_id bigint,
              error_severity text,
              sql_state_code text,
              message text,
              detail text,
              hint text,
              internal_query text,
              internal_query_pos integer,
              context text,
              query text,
              query_pos integer,
              location text,
              application_name text
          ) server pglog
          options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       elsif logversion < 14 then
          /* Postgres 13 added the backend_type column */
          execute format(
            $$create foreign table %I.%I (
                log_time timestamp(3) with time zone,
                user_name text,
                database_name text,
                process_id integer,
                connection_from text,
                session_id text,
                session_line_num bigint,
                command_tag text,
                session_start_time timestamp with time zone,
                virtual_transaction_id text,
                transaction_id bigint,
                error_severity text,
                sql_state_code text,
                message text,
                detail text,
                hint text,
                internal_query text,
                internal_query_pos integer,
                context text,
                query text,
                query_pos integer,
                location text,
                application_name text,
                backend_type text
            ) server pglog
            options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       else
          /* Postgres 14 has inherited the backend_type column from Postgres 13
           * and added the leader_pid and the query_id columns.*/
         execute format(
           $$create foreign table %I.%I (
               log_time timestamp(3) with time zone,
               user_name text,
               database_name text,
               process_id integer,
               connection_from text,
               session_id text,
               session_line_num bigint,
               command_tag text,
               session_start_time timestamp with time zone,
               virtual_transaction_id text,
               transaction_id bigint,
               error_severity text,
               sql_state_code text,
               message text,
               detail text,
               hint text,
               internal_query text,
               internal_query_pos integer,
               context text,
               query text,
               query_pos integer,
               location text,
               application_name text,
               backend_type text,
               leader_pid integer,
               query_id bigint
           ) server pglog
           options ( filename %L, format 'csv' )$$, schema_name, fdw_name, directory || '/' || filename);
       end if;
      execute format(
        $$select min(log_time) as min_time,
          (max(log_time) + interval '1ms') as max_time
        from %I.%I$$, schema_name, fdw_name) into bounds;
      /* There's not need to add an empty table as a partition + which bound
       * would you pick for the empty partition? */
      if bounds.min_time is not null
      then
        execute format($$alter table %I.pglog attach partition %I.%I for values from (%L) to (%L)$$,
          schema_name,
          schema_name,
          fdw_name,
          bounds.min_time,
          bounds.max_time);
      end if;
    else
      raise notice $$There is already an object called % in schema %.$$, fdw_name, schema_name;
    end if;
  end;
$body$
language plpgsql;

/* this procedure will create the logtable in the given schema */
create or replace procedure pglog.create_logtable(schema_name text,
  logversion integer) as
$body$
  begin
    if logversion < 13 then
      /* The following lines will be removed when Postgres 12 won't be
       * supported anymore */
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text
        ) partition by range (log_time)$$, schema_name);
    elsif logversion < 14 then
      /* Postgres 13 added the backend_type column */
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text,
          backend_type text
        ) partition by range (log_time)$$, schema_name);
    else
       /* Postgres 14 has inherited the backend_type column from Postgres 13
        * and added the leader_pid and the query_id columns.*/
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text,
          backend_type text,
          leader_pid integer,
          query_id bigint
        ) partition by range (log_time)$$, schema_name);
    end if;
  end;
$body$
language 'plpgsql';

/* This procedure will create several logtables for all csv files inside the
 * given directory.
 * If the materialized boolean is false, it will create FDWs
 * If the materialized boolean is true, it will create real tables */
create or replace procedure pglog.create_logtable_from_dir(
  directory text,
  logversion integer,
  schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    my_files record;
    my_function text;
    is_new boolean := false;
  begin
    if not pglog.is_object_already_there(schema_name, 'pglog')
    then
      execute format($$call pglog.create_logtable(%L,%L::integer)$$, schema_name, logversion);
        is_new := true;
    end if;
    if materialized
    then
      for my_files in
        execute format($$select filename from pg_ls_dir(%L) as filename where filename ~ '.csv'$$, directory)
      loop
        execute format($$call pglog.create_real_table_from_file(%L, %L, %L::integer, %L)$$, my_files.filename, directory, logversion, schema_name);
      end loop;
      if is_new
      then
        execute format($$analyze %I.pglog$$, schema_name);
        execute format($$create index on %I.pglog(error_severity)$$, schema_name);
      end if;
    else
      for my_files in
        execute format($$select filename from pg_ls_dir(%L) as filename where filename ~ '.csv'$$, directory)
      loop
        execute format($$call pglog.create_foreign_table_from_file(%L, %L, %L::integer, %L)$$, my_files.filename, directory, logversion, schema_name);
      end loop;
    end if;
    execute format($$call pglog.create_all_views(%L, %L::boolean)$$, schema_name, materialized);
  end;
$body$
language plpgsql;

/* This procedure will create a logtable from a given csv file 
 * If the materialized boolean is false, it will create a FDW
 * If the materialized boolean is true, it will create a real table */
create or replace procedure pglog.create_logtable_from_file(
  filename text,
  directory text,
  logversion integer,
  schema_name text default 'pglog',
  materialized boolean default false
  ) as
$body$
  declare
    is_new boolean := false;
  begin
    if not pglog.is_object_already_there(schema_name, 'pglog')
    then
      execute format($$call pglog.create_logtable(%L,%L::integer)$$, schema_name, logversion);
        is_new := true;
    end if;
    if materialized
    then
      execute format($$call pglog.create_real_table_from_file(%L, %L, %L::integer, %L)$$, filename, directory, logversion, schema_name);
      if is_new
      then
        execute format($$create index on %I.pglog(error_severity)$$, schema_name);
        raise notice $$For performance reason, you might want to vacuum %.pglog after load$$, schema_name;
      end if;
    else
      execute format($$call pglog.create_foreign_table_from_file(%L, %L, %L::integer, %L)$$, filename, directory, logversion, schema_name);
    end if;
    execute format($$call pglog.create_all_views(%L, %L::boolean)$$, schema_name, materialized);
  end;
$body$
language 'plpgsql';

/* This pricedure will create log tables for all the logfiles in csv format
 * found in the log_destination dirrectory.*/
create or replace procedure pglog.create_mylogs(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    directory text;
    log_destination text;
    logging_collector boolean;
    logversion integer;
  begin
    execute $$select setting::integer/10000 from pg_settings where name = 'server_version_num'$$ into logversion;
    execute $$select setting from pg_settings where name = 'log_destination'$$ into log_destination;
    if log_destination <> 'csvlog' then
      raise exception $$The only acceptable value for the 'log_destination' parameter is 'csvlog'$$;
    end if;
    execute $$select setting from pg_settings where name = 'logging_collector'$$ into logging_collector;
    if not logging_collector then
      raise exception $$The only acceptable value for the 'logging_collector' parameter is 'true'$$;
    end if;
    execute $$select setting from pg_settings where name = 'log_directory'$$ into directory;
    execute format($$call pglog.create_logtable_from_dir(%L,%L::integer,%L,%L::boolean)$$, directory, logversion, schema_name, materialized);
  end;
$body$
language plpgsql;

/* This procdure will import the data from a given csv file into a table */
create or replace procedure pglog.create_real_table_from_file(filename text,
  directory text,
  logversion integer,
  schema_name text default 'pglog') as
$body$
  declare
    table_name text;
    bounds record;
  begin
    execute format($$select lower(regexp_replace(regexp_replace(%L, '\.csv$',''),'\W+',''))$$, filename) into table_name;
    if not pglog.is_object_already_there(schema_name, table_name) then
      if logversion < 13 then
        /* The following lines will be removed when Postgres 12 won't be
         * supported anymore */
        execute format(
          $$create table %I.%I (
              log_time timestamp(3) with time zone,
              user_name text,
              database_name text,
              process_id integer,
              connection_from text,
              session_id text,
              session_line_num bigint,
              command_tag text,
              session_start_time timestamp with time zone,
              virtual_transaction_id text,
              transaction_id bigint,
              error_severity text,
              sql_state_code text,
              message text,
              detail text,
              hint text,
              internal_query text,
              internal_query_pos integer,
              context text,
              query text,
              query_pos integer,
              location text,
              application_name text
          ) $$, schema_name, table_name);
     elsif logversion < 14 then
        /* Postgres 13 added the backend_type column */
        execute format(
          $$create table %I.%I (
              log_time timestamp(3) with time zone,
              user_name text,
              database_name text,
              process_id integer,
              connection_from text,
              session_id text,
              session_line_num bigint,
              command_tag text,
              session_start_time timestamp with time zone,
              virtual_transaction_id text,
              transaction_id bigint,
              error_severity text,
              sql_state_code text,
              message text,
              detail text,
              hint text,
              internal_query text,
              internal_query_pos integer,
              context text,
              query text,
              query_pos integer,
              location text,
              application_name text,
              backend_type text
          ) $$, schema_name, table_name);
     else
        /* Postgres 14 has inherited the backend_type column from Postgres 13
         * and added the leader_pid and the query_id columns.*/
        execute format(
          $$create table %I.%I (
              log_time timestamp(3) with time zone,
              user_name text,
              database_name text,
              process_id integer,
              connection_from text,
              session_id text,
              session_line_num bigint,
              command_tag text,
              session_start_time timestamp with time zone,
              virtual_transaction_id text,
              transaction_id bigint,
              error_severity text,
              sql_state_code text,
              message text,
              detail text,
              hint text,
              internal_query text,
              internal_query_pos integer,
              context text,
              query text,
              query_pos integer,
              location text,
              application_name text,
              backend_type text,
              leader_pid integer,
              query_id bigint
          ) $$, schema_name, table_name);
     end if;
      execute format($$copy %I.%I from %L with csv quote as '"'$$, schema_name, table_name, directory || '/' || filename);
      execute format(
        $$select min(log_time) as min_time,
          (max(log_time) + interval '1ms') as max_time
        from %I.%I$$, schema_name, table_name) into bounds;
      /* There's not need to add an empty table as a partition + which bound
       * would you pick for the empty partition? */
      if bounds is not null
      then
        execute format($$alter table %I.pglog attach partition %I.%I for values from (%L) to (%L)$$,
          schema_name,
          schema_name,
          table_name,
          bounds.min_time,
          bounds.max_time);
      end if;
    else
      raise notice $$There is already an object called % in schema %.$$, fdw_name, schema_name;
    end if;
  end;
$body$
language plpgsql;

create or replace procedure pglog.create_view_autovacuum_report(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'autovacuum_report') then
      execute format($$drop %s view %I.autovacuum_report$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.autovacuum_report as (
        with autovacuum(schema_name, table_name, session_id) as (
          select
            split_part
            (
              split_part(message, '"', 2),
              '.',
              2
            ) as schema_name,
            split_part
            (
              split_part(message, '"', 2),
              '.',
              3
            ) as table_name,
            session_id
          from %I.pglog
          where error_severity = 'LOG'
            and message ~  '^automatic vacuum of table '
        ),
        autoanalyze(schema_name, table_name, session_id) as (
          select
            split_part
            (
              split_part(message, '"', 2),
              '.',
              2
            ) as schema_name,
            split_part
            (
              split_part(message, '"', 2),
              '.',
              3
            ) as table_name,
            session_id
          from %I.pglog
          where error_severity = 'LOG'
            and message ~  '^automatic analyze of table '
        ),
        stat_vacuum(schema_name, table_name, vacuum_count) as (
          select schema_name,
            table_name,
            count(*) as vacuum_count
          from autovacuum
          where table_name is not null
          group by schema_name,
           table_name
        ),
        stat_analyze(schema_name, table_name, analyze_count) as (
          select schema_name,
            table_name,
            count(*) as analyze_count
          from autoanalyze
          where table_name is not null
          group by schema_name,
           table_name
        )
        select
          /* As we can don't know if we analyze more than we vacuum and vice
           * versa, we have to find out which table and schema names are not
           * null */
          coalesce(stat_analyze.schema_name, stat_vacuum.schema_name) as schema_name,
          coalesce(stat_analyze.table_name, stat_vacuum.table_name) as table_name,
          coalesce(vacuum_count, 0) as vacuum_count,
          coalesce(analyze_count, 0) as analyze_count
        from stat_vacuum
          /* Normaly we analyze a table more frequently than we vacuum it,
           * so we have to left join */
          full join
            stat_analyze
          on stat_vacuum.schema_name = stat_analyze.schema_name
          and stat_vacuum.table_name = stat_analyze.table_name
            order by vacuum_count desc, analyze_count desc)$$, mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;

create or replace procedure pglog.create_view_checkpoints_stats(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'checkpoints_stats') then
      execute format($$drop %s view %I.checkpoints_stats$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.checkpoints_stats as (
        with checkpoint_start(rnum, process_id, log_time, session_line_num, message) as
        (select
          row_number() over (order by log_time, process_id, session_line_num) as rnum,
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint starting: time'
        ),
        checkpoint_stop(process_id, log_time, session_line_num, message) as
        (select
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint complete:'
        )
        select
          avg(s2.log_time - s1.log_time) as avg_checkpoint_freq,
          percentile_cont(0.5) within group(order by s2.log_time - s1.log_time) as median_checkpoint_freq,
          avg(e1.log_time - s1.log_time) as avg_checkpoint_duration,
          percentile_cont(0.5) within group(order by e1.log_time - s1.log_time) as median_checkpoint_duration
        from checkpoint_start s1
          inner join
            checkpoint_start s2
          on s1.rnum + 1 = s2.rnum
          inner join
            checkpoint_stop e1
          on s1.process_id = e1.process_id
          and s1.session_line_num + 1 = e1.session_line_num
          inner join
            checkpoint_stop e2
          on s2.process_id = e2.process_id
          and s2.session_line_num + 1 = e2.session_line_num)$$,
       mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;
create or replace procedure pglog.create_view_checkpoints_stats_by_hour(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'checkpoints_stats_by_hour') then
      execute format($$drop %s view %I.checkpoints_stats_by_hours$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.checkpoints_stats_by_hour as (
        with checkpoint_start(rnum, process_id, log_time, session_line_num, message) as
        (select
          row_number() over (order by log_time, process_id, session_line_num) as rnum,
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint starting: time'
        ),
        checkpoint_stop(process_id, log_time, session_line_num, message) as
        (select
          process_id,
          log_time,
          session_line_num,
          message
        from %I.pglog
        where message ~ '^checkpoint complete:'
        )
        select
          extract(hour from s2.log_time),
          avg(s2.log_time - s1.log_time) as avg_checkpoint_freq,
          percentile_cont(0.5) within group(order by s2.log_time - s1.log_time) as median_checkpoint_freq,
          avg(e1.log_time - s1.log_time) as avg_checkpoint_duration,
          percentile_cont(0.5) within group(order by e1.log_time - s1.log_time) as median_checkpoint_duration
        from checkpoint_start s1
          inner join
            checkpoint_start s2
          on s1.rnum + 1 = s2.rnum
          inner join
            checkpoint_stop e1
          on s1.process_id = e1.process_id
          and s1.session_line_num + 1 = e1.session_line_num
          inner join
            checkpoint_stop e2
          on s2.process_id = e2.process_id
          and s2.session_line_num + 1 = e2.session_line_num
         group by extract(hour from s2.log_time))$$,
       mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;
create or replace procedure pglog.create_view_error_report(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'error_report ') then
      execute format($$drop %s view %I.error_report $$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.error_report as (select
        error_severity,
        count(*) as nb,
        round(100. * count(*) / sum(count(*)) over (),2)
      from %I.pglog
        inner join
        (values (0, 'PANIC'),
          (1,'FATAL'),
          (2,'ERROR'),
          (3,'WARNING'),
          (4,'LOG'),
          (5,'NOTICE'),
          (6,'INFO'),
          (7,'DEBUG')) as severity(level,name)
        on %I.pglog.error_severity = severity.name
      group by error_severity,
        severity.level
      order by severity.level)$$, mat, schema_name, schema_name, schema_name);
  end
$body$
language plpgsql;

create or replace procedure pglog.create_view_tempfile_queries(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'tempfile_queries') then
      execute format($$drop %s view %I.tempfile_queries$$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.tempfile_queries as (
        with temporary_file(session_id, session_line_num, size, query) as (
          select session_id,
            session_line_num,
            /* Doc specifies that should no unit be there, the size is in kB */
            split_part(message, ', size ', 2)::bigint*1024,
            /* remove hard values (number) */
            regexp_replace
            (
              /* remove hard values (quote delimited) */
              regexp_replace
              (
                /* remove comments */
                regexp_replace
                (
                  query,
                  '[ \t\r\n\v\f]*/\*.+\*/[ \t\r\n\v\f]*',
                  ''
                ),
                E'\'.+\'',
                '?'
              ),
              '=[ \t\r\n\v\f]*[0-9\.]+',
              '= ?'
            )
          from %I.pglog
          where error_severity = 'LOG'
            and message ~ '^temporary file:.*, size '
        )
        select count(*) as frequency,
          pg_size_pretty(percentile_disc(0.5) within group (order by size)) as query_tempfilesize_median,
          pg_size_pretty(avg(size)) as query_tempfilesize_average,
          pg_size_pretty(count(*) * avg(size)) as total_size,
          query
        from temporary_file
        group by query
        order by count(*) * avg(size) desc)$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;
create or replace procedure pglog.create_view_tempfile_stats(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'tempfile_report ') then
      execute format($$drop %s view %I.tempfile_report $$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.tempfile_report as (
        with temporary_file(size) as (
          /* Doc specifies that should no unit be there, the size is in kB */
          select split_part(message, ', size ', 2)::bigint*1024
          from %I.pglog
          where error_severity = 'LOG'
            and message ~ '^temporary file:.*, size '
        )
        select
          pg_size_pretty(min(size)) as min,
          pg_size_pretty(max(size)) as max,
          pg_size_pretty(avg(size)) as global_average,
          pg_size_pretty(percentile_disc(0.1) within group (order by size)) as "10%%",
          pg_size_pretty(percentile_disc(0.2) within group (order by size)) as "20%%",
          pg_size_pretty(percentile_disc(0.3) within group (order by size)) as "30%%",
          pg_size_pretty(percentile_disc(0.4) within group (order by size)) as "40%%",
          pg_size_pretty(percentile_disc(0.5) within group (order by size)) as "50%%",
          pg_size_pretty(percentile_disc(0.6) within group (order by size)) as "60%%",
          pg_size_pretty(percentile_disc(0.7) within group (order by size)) as "70%%",
          pg_size_pretty(percentile_disc(0.8) within group (order by size)) as "80%%",
          pg_size_pretty(percentile_disc(0.9) within group (order by size)) as "90%%",
          pg_size_pretty(percentile_disc(1) within group (order by size)  ) as "100%%"
        from
          temporary_file)$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;
create or replace procedure pglog.create_view_top_n_query(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'top_n_queries') then
      execute format($$drop %s view %I.top_n_queries$$, mat, schema_name);
    end if;
    execute format($$create %s view %I.top_n_queries as (
      with queries as (
      select
        split_part(message, ' ', 2)::decimal as duration,
        regexp_replace
        (
          regexp_replace
          (
            regexp_replace
            (
              regexp_replace
              (
                /* get only the query */
                substring(message from '^(?:duration: \d+.\d+ ms  )?statement: (.*)'),
                /* remove comment */
                '(--[^\n]*)|((\/\*).*(\*\/))',
                ' ',
                'g'
              ),
              /* replace all new lines and unused blanks*/
              '[\r\n]+|\s{2,}|\s$',
              ' ',
              'g'
            ),
            /* Remove string and numeric constant values */
            $text$E?'.*'|(\$.*\$).*\1|\W(-?([0-9]+\.?[0-9]*)|(\.[0-9]+))\W$text$,
            '?',
            'g'
          ),
          '\s\s+',
          ' ',
          'g'
        ) as query
      from
        %I.pglog 
      where
        error_severity = 'LOG'
      and message ~ '^duration'
      and message ~ 'statement'
      and command_tag not in ('BEGIN','END','COMMIT')
    )
    select query,
      count(1),
      avg(duration) as average,
      percentile_disc(0.5) within group (order by duration) as median
    from queries
    group by query
    order by average desc
    )$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;

create or replace procedure pglog.create_views_error_analyze(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
    severity text[];
    label text;
  begin
    severity := array['WARNING', 'ERROR', 'FATAL', 'PANIC'];
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    foreach label in array severity
    loop
      if pglog.is_object_already_there(schema_name, 'error_analyze' || lower(label)) then
        execute format($$drop %s view %I.error_report_%s $$, mat, schema_name, lower(label));
      end if;
      execute format(
        $$create %s view %I.error_analyze_%s as (select
            message,
            count(1),
            min(log_time),
            max(log_time)
          from
            %I.pglog
          where error_severity = %L
          group by message
          order by count(1) desc)$$,
        mat, schema_name, lower(label), schema_name, label);
    end loop;
  end
$body$
language plpgsql;


