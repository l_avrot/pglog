Here's how to play with pglog on a docker container

# Get the Dockerfile

```
wget https://gitlab.com/l_avrot/pglog/-/raw/master/docker/Dockerfile
```

# Build the image

```
docker build -t pglog_postgresql .
```

# Run the docker container (in the background)

```
$# docker run -d --rm -P --name  pglog pglog_postgresql
```

# Connect to the container
From your desktop/laptop:
```
$# docker ps -f name=pglog
CONTAINER ID   IMAGE           COMMAND                  CREATED          STATUS PORTS                     NAMES
c009ea8515c0   my_postgresql   "/usr/lib/postgresql…"   49 seconds ago   Up 48 seconds   0.0.0.0:55003->5432/tcp   pglog
$# psql -h localhost -p 55003 laetitia
```

From the container itself:
```
$# docker exec -it pglog /bin/psql
psql (14.0 (Ubuntu 14.0-1.pgdg20.04+1))
Type "help" for help.

postgres=# \c laetitia
You are now connected to database "laetitia" as user "postgres".
laetitia=#
```

# Analyzing logs
First copy the logs into the docker container and adjust file permissions.

```
$# docker cp <your log> pglog:/usr/share/pglog
$# docker exec -u 0 -it pglog chown postgres:postgres /usr/share/pglog/<your log>
```

Then, go into the container and create the pglog table:

```
$# docker exec -it pglog /bin/psql
psql (14.0 (Ubuntu 14.0-1.pgdg20.04+1))
Type "help" for help.

postgres=# \c laetitia
laetitia=# call pglog.create_logtable_from_dir('/usr/share/pglog', <your log version>, <your schema>, <materialized>);
```

If you used materialized, we recommend vacuuming.

```
laetitia=# vacuum analyze pglog
```

See your views or mat views:

```
laetitia=# \dmv pglog.*
                         List of relations
 Schema |           Name            |       Type        |  Owner   
--------+---------------------------+-------------------+----------
 pglog  | autovacuum_report         | materialized view | postgres
 pglog  | checkpoints_stats         | materialized view | postgres
 pglog  | checkpoints_stats_by_hour | materialized view | postgres
 pglog  | connections_report        | materialized view | postgres
 pglog  | error_analyze_error       | materialized view | postgres
 pglog  | error_analyze_fatal       | materialized view | postgres
 pglog  | error_analyze_panic       | materialized view | postgres
 pglog  | error_analyze_warning     | materialized view | postgres
 pglog  | error_report              | materialized view | postgres
 pglog  | sessions_stats            | materialized view | postgres
 pglog  | tempfile_queries          | materialized view | postgres
 pglog  | tempfile_report           | materialized view | postgres
 pglog  | top_n_queries             | materialized view | postgres
(13 rows)
```

# Stop the container

```
$# docker stop pglog
```

This will also remove the container as it was created using the `--rm` option.
