EXTENSION = pglog
VERS = 3.0
SQL_FILE = pglog--$(VERS).sql
DATA = pglog--3.0.sql \
	   pglog--2.2.sql \
	   pglog--2.2--3.0.sql

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

all: build

build: 
	rm -f $(SQL_FILE)
	cat sql/header.sql > $(SQL_FILE)
	for f in sql/functions/*.sql; do cat $$f >> $(SQL_FILE); done
	for f in sql/proc/*.sql; do cat $$f >> $(SQL_FILE); done
